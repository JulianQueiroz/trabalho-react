import {Image} from "react-native"

export const style = {

    colors:{
        gray:'#2B2C40',
        yellow:'#FFB803',
        blue: '#000118',
        white: '#FFFFFF',
        loginPage: '#D9D9D9',
        black: '#000000'
    },
    
}
