import styled from 'styled-components/native';
import { style } from '../../Global/global';


export const InputSection = styled.View`
padding-left: 29px;
padding-right: 29px;
`
export const EmailSection = styled.View`

margin-bottom: 10px;
`
export const Email = styled.Text`
font-size: 15px;
color:${style.colors.black};
font-weight: bold;
`
export const TextInput = styled.TextInput`
width:317px;
height:56px;
border-radius: 15px;
border-width: 1;
border-color:#000000c7;

`
export const SenhaSection = styled.View`
margin-top: 49px;
`
export const Senha = styled.Text`
margin-bottom: 10px;
font-size: 15px;
color:${style.colors.black};
font-weight: bold;

`


