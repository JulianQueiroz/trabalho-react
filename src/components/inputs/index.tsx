import React from "react";
import {Image, View} from "react-native";
import {InputSection,Email,Senha,SenhaSection,EmailSection,TextInput} from "./style";

export function Input() {
    return(
        <InputSection>
            <EmailSection>
                <Email>
                     E-mail:
                </Email>
            </EmailSection>
            
            <TextInput 
                placeholder="Insira seu email"
                // style={{borderWidth:1,
                // borderColor:'#000000c7',
                // height:56,
                // borderRadius:15}}
            />
            <SenhaSection>
                <Senha>
                    Senha:
                </Senha>
            </SenhaSection>
            
            <TextInput placeholder="Insira sua senha"
            //  style={{borderWidth:1,
            //     borderColor:'#000000c7',
            //     height:56,
            //     borderRadius:15}}
            />
        </InputSection>
    )
}

export default Input;