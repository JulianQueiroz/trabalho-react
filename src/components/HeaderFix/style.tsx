import styled from 'styled-components/native';
import { style } from '../../Global/global';

export const LogoBG = styled.Image`
width: 138px;
height: 48px;
margin-left:19%;
margin-top: 1%;

`
export const HeaderFix = styled.View`
background-color:${style.colors.gray};
width:375px;
height: 54px;
flex-direction: row;
`
export const Menu = styled.View`

margin-top: 18px;
flex-direction: row;

`
export const MenuIcon = styled.Image`
width: 19px;
height: 11px;
margin-left: 26px;
margin-top: 5px;
`
export const Search = styled.View`
flex-direction: row;
margin-left: 78px;
margin-top: 19px;
`
export const SearchIcon = styled.Image`
width: 17px;
height: 16px;
`