import React from "react";
import {Image, View } from "react-native";
import {HeaderFix,LogoBG,Menu,MenuIcon,Search,SearchIcon} from "./style";


export function HeaderBG() {
    return (
        <HeaderFix>
        <Menu>
            <MenuIcon source={require('../../assets/menu/Vector.png')}/>
        </Menu>
        <LogoBG source={require('../../assets/Logo/Tecno Pro (2).png')} />
        <Search>
            <SearchIcon source={require('../../assets/search/Vector.png')}/>
        </Search>
    </HeaderFix>
    )
}