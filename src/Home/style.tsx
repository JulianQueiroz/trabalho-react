import styled from 'styled-components/native';
import { style } from '../Global/global'

export const Container = styled.View`
background-color:${style.colors.blue};
flex:1;
`

export const BannerBG = styled.Image`
width:375px;
height: 170px;
`
export const MaisVendidos = styled.Text`
color:${style.colors.white};
margin-left: 20px;
margin-top: 17px;
`
export const Produtos = styled.View`
margin-left:20px;
margin-top: 27px;


`
export const Prod1 = styled.View`
flex-direction: row;
margin-bottom:40px;
`
export const PlacaVideo = styled.Image`
width: 94px;
height: 71px;
display: flex;
`
export const PlacaVideoTextPrice = styled.View `
flex-direction: column;
margin-left: 15px;
font-weight: bold;
`
export const PlacaVText = styled.Text`
color:${style.colors.white};
font-size: 13px;
display: flex;
margin-bottom: 18px;

`
export const PlacaPrice = styled.Text`
font-size: 18px;
color:${style.colors.yellow};
font-weight: bold;

`
export const Prod2 = styled.View`
flex-direction: row;
margin-bottom:40px;
`
export const Ssd = styled.Image`
width: 94px;
height: 71px;
display: flex;
`
export const SsdTextPrice = styled.View`
flex-direction: column;
margin-left: 15px;
font-weight: bold;
`
export const SsdText = styled.Text `
color:${style.colors.white};
font-size: 13px;
display: flex;
margin-bottom: 18px;
`
export const SsdPrice = styled.Text `
font-size: 18px;
color:${style.colors.yellow};
font-weight: bold;
`
export const Prod3 = styled.View`
flex-direction: row;
margin-bottom:40px;

`
export const Macbook = styled.Image`
width: 94px;
height: 71px;
display: flex;
`
export const MacbookTextPrice = styled.View`
flex-direction: column;
margin-left: 15px;
font-weight: bold;
`
export const MacbookText = styled.Text `
color:${style.colors.white};
font-size: 13px;
display: flex;

`
export const MacbookPrice = styled.Text `
font-size: 18px;
color:${style.colors.yellow};
margin-top: 3px;
font-weight: bold;
`
export const Prod4 = styled.View`
flex-direction: row;

margin-bottom: 3px;

`
export const PlacaMae = styled.Image`
width: 94px;
height: 71px;
display: flex;
`
export const PlacaMaeTextPrice = styled.View`
flex-direction: column;
margin-left: 15px;
`
export const PlacaMaeText = styled.Text `
color:${style.colors.white};
font-size: 13px;
display: flex;
`
export const PlacaMaePrice = styled.Text `
font-size: 18px;
color:${style.colors.yellow};
margin-top: 2px;
font-weight: bold;

`
export const Prod5 = styled.View `
flex-direction: row;

`
export const ImgSeta = styled.Image `
width: 54px;
height: 53px;
margin-left: 267px;
margin-top: 23px;

`
