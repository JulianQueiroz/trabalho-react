import React from "react";
import {Image, View } from "react-native";
import {Container,BannerBG, MaisVendidos, Produtos, PlacaVideo,Ssd, PlacaVText, Prod1,PlacaPrice, PlacaVideoTextPrice,
    Prod2,SsdTextPrice,SsdText,SsdPrice, Prod3,Macbook,MacbookTextPrice,MacbookText,MacbookPrice,Prod4,PlacaMae,PlacaMaeTextPrice,PlacaMaeText,PlacaMaePrice,
Prod5,ImgSeta} from "./style";
import {HeaderBG} from '../components/HeaderFix';

function Home (){
    return(
        <Container>
           <HeaderBG />
            <BannerBG source={require('../assets/banner principal/Frame 1.png')}/>
            <MaisVendidos>
                Mais vendidos
            </MaisVendidos>
            <Produtos>
                <Prod1>
                    <PlacaVideo source={require('../assets/placa de video/Group 4.png')}/>
                    <PlacaVideoTextPrice>
                    <PlacaVText>
                    Placa de Vídeo Palit NVIDIA GeForce GTX{'\n'}1050 Ti StormX, 4GB, GDDR5, 128bit
                    </PlacaVText>
                    <PlacaPrice>
                        R$ 1099,99
                    </PlacaPrice>
                    </PlacaVideoTextPrice>
                </Prod1>
                <Prod2>
                    <Ssd source={require('../assets/ssd/Group 5.png')}/>
                    <SsdTextPrice>
                        <SsdText>
                        SSD WD Green, 240GB, SATA, Leitura{'\n'}545MB/s
                        </SsdText>
                        <SsdPrice>
                            R$ 187,99
                        </SsdPrice>
                    </SsdTextPrice>
                </Prod2>
                <Prod3>
                    <Macbook source={require('../assets/macbook/Group 8.png')}/>
                    <MacbookTextPrice>
                        <MacbookText>
                        Apple MacBook Pro MD313LL/A de 13{'\n'}polegadas (4 GB de RAM, 500 GB de HD{'\n'}(USADO)
                        </MacbookText>
                        <MacbookPrice>
                            R$ 187,99
                        </MacbookPrice>
                    </MacbookTextPrice>
                </Prod3>
                <Prod4>
                    <PlacaMae source={require('../assets/placa mae/Group 7.png')}/>
                    <PlacaMaeTextPrice>
                        <PlacaMaeText>
                        Placa-Mãe MSI A320M-A Pro{'\n'}Max p/ AMD AM4, m-ATX, DDR4{'\n'}(RENOVADO)
                        </PlacaMaeText>
                        <PlacaMaePrice>
                            R$ 187,99
                        </PlacaMaePrice>
                    </PlacaMaeTextPrice>
                </Prod4>
                <Prod5>
                    <ImgSeta source={require('../assets/seta/Group 26.png')}/>
                </Prod5>
            </Produtos>
        </Container>
        
    );
}
export default Home;