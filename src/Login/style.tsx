import styled from 'styled-components/native';
import { style } from '../Global/global'

export const Container = styled.View`
background-color:${style.colors.loginPage};
flex:1;
`
export const LoginSection = styled.View`
align-items: center;
margin-top: 60px;
margin-bottom: 49px;
`

export const LoginText= styled.Text`
color:${style.colors.black};
font-size: 28px;
font-weight: bold;
`

export const ButtonSubmit = styled.TouchableOpacity`
width:317px;
height:56px;
border-radius: 15px;
align-items: center;
background-color: #FFB803;
margin-left: 29px;
margin-top:52px;
font-weight: bold;

`
export const TextSubmit = styled.Text`
margin-top: 17px;
font-weight: bold;

`
export const JoinUs = styled.View `
flex-direction: row;
justify-content: center;
margin-top: 49px;

`
export const JoinUsText1 = styled.Text`
font-size: 15px;
`

export const JoinUsText2 = styled.Text `
font-size:15px;
font-weight: bold;
text-decoration-line: underline;
margin-left:5px;
`