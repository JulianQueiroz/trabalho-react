import React from "react";
import {Image, View,TextInput,TouchableOpacity } from "react-native";
import {Container,LoginSection,LoginText,ButtonSubmit,TextSubmit,JoinUs,JoinUsText1,JoinUsText2} from  "./style";
import {HeaderBG} from '../components/HeaderFix';
import {Input} from '../components/inputs'


function Login () {
    return (
        <Container>
            <HeaderBG/>
            <LoginSection>
                <LoginText>
                    Login do Cliente
                </LoginText>
            </LoginSection>
            <Input/>
            <ButtonSubmit>
                <TextSubmit>
                    Enviar
                </TextSubmit>
            </ButtonSubmit>
            <JoinUs>
                <JoinUsText1>
                    Não tem cadastro?    
                    </JoinUsText1>
                <JoinUsText2>
                    Cadastre-se!
                </JoinUsText2>
            </JoinUs>
        </Container>

    )
}
export default Login;